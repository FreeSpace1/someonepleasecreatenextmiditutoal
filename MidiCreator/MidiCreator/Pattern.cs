﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi.DataElement;

namespace MidiCreator
{
    class Pattern
    {
		private MeasureCollection measureCollection;
		internal List<MidiEvent> toMidiEventList() {
			return this.measureCollection.toMidiEventList();
		}

		public Pattern(MeasureCollection measureCollection) : this() {
			this.measureCollection = measureCollection;
		}

		public Pattern() {
			this.measureCollection = new MeasureCollection();
		}

		public Pattern(Tick tick): this() {
			this.measureCollection = new MeasureCollection(tick);
		}

		public Pattern createBass() {
			this.measureCollection.createBass();
			return this;
		}

		public List<MeasureBase> toMeasureList() {
			return this.measureCollection.toMeasureList();
		}

	}
}
