﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi.Data;

namespace MidiCreator {
	class PeriodMeasure : MeasureBase {

		public PeriodMeasure(MidiEventCollection midiEventCollection, ResolutionSource resolutionSource) : base(midiEventCollection, resolutionSource) { }

		public PeriodMeasure(MeasureBase measureBass, Tick tick) : base(measureBass, tick) { }
		public PeriodMeasure() : base() { }

		public override MeasureBase createBass() {
			Random random = new Random();
			while (this.isOver1Measure()) {
				Note note = new Note((random.Next(Constant.SLAP_BASS_1_MIN, Constant.SLAP_BASS_1_MAX)));
				Velocity velocity = new Velocity((byte)(127 * random.Next(1)));
				Speed speed = new Speed(127);
				MidiEventFactory midiEventFactory = new MidiEventFactory(note, velocity, speed, this.midiEventCollection.endOfTick);
				this.midiEventCollection.add(midiEventFactory.NoteEvent_Random);
			}
			return this;
		}
	}
}
