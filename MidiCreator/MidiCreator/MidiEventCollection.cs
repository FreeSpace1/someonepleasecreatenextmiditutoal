﻿using NextMidi.Data.Track;
using NextMidi.DataElement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MidiCreator {
	class MidiEventCollection {
		private List<MidiEvent> value;
		public MidiEventCollection(MidiEvent midiEvent) : this() {
			this.value.Add(midiEvent);
		}
		public MidiEventCollection() {
			this.value = new List<MidiEvent>();
		}
		public MidiEventCollection(MidiEventCollection midiEventCollection, MidiEvent midiEvent) : this(midiEventCollection) {
			this.value.Add(midiEvent);
		}

		public MidiEventCollection(MidiEventCollection midiEventCollection, MidiEventCollection midiEventCollection2) : this(midiEventCollection) {
			this.value.AddRange(midiEventCollection2.value);
		}

		public MidiEventCollection(MidiEventCollection midiEventCollection) {
			this.value = midiEventCollection.value;
		}
		public MidiEventCollection(MidiEventCollection midiEventCollection, Tick tick) : this(midiEventCollection) {
			foreach (var midiEvent in this.value) {
				midiEvent.Tick = new Tick(midiEvent, tick).toInt();
			}
		}

		public MidiEvent add(MidiEvent midiEvent) {
			this.value.Add(midiEvent);
			return midiEvent;
		}

		internal IEnumerable<MidiEvent> toMidiEventList() {
			return this.value;
		}
		public MidiTrack createMidiTrack() {
			return new MidiTrack(this.value);
		}

		internal int differenceOfEndAndFirst() {
			return this.endOfTick.toInt() - this.firstMidiEvent.Tick;
		}
		internal int differenceOfLastAndFirst() {
			return this.lastMidiEvent.Tick - this.firstMidiEvent.Tick;
		}

		public List<MidiEvent> toList() {
			return this.value;
		}
		public MidiEvent lastMidiEvent{
			get {
				MidiEvent midiEvent = this.value.First();
				foreach (var midiEventElement in this.value) {
					midiEvent = this.getBack(midiEvent, midiEventElement);
				}
				return midiEvent;
			}
		}
		public MidiEvent firstMidiEvent {
			get{
				MidiEvent midiEvent = this.value.First();
				foreach (var midiEventElement in this.value) {
					midiEvent = this.getForward(midiEvent, midiEventElement);
				}
				return midiEvent;

			}

		}
		private MidiEvent getBack(MidiEvent midiEvent1, MidiEvent midiEvent2) {
			if (midiEvent1.Tick < midiEvent2.Tick) {
				return midiEvent2;
			}
			return midiEvent1;
		}
		private MidiEvent getForward(MidiEvent midiEvent1, MidiEvent midiEvent2) {
			if (midiEvent1.Tick > midiEvent2.Tick) {
				return midiEvent2;
			}
			return midiEvent1;
		}

		public Tick endOfTick {
			get {
				if (this.value.Count.Equals(0)) {
					return new Tick(0);
				}
				var note = (NoteEvent)(this.lastMidiEvent);
				return new Tick(note.Tick + note.Gate);
			}											 
		}

	}
}
