﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi;
using NextMidi.DataElement.MetaData;
using NextMidi.DataElement;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.Filing.Midi;
using NextMidi.Filing.Midi.MidiFile;

namespace MidiCreator {
	class Program {
		static void Main(string[] args) {



			MidiEventCollection midiEventCollection = new MidiEventCollection(new ProgramEvent(0));
			midiEventCollection = new MidiEventCollection(midiEventCollection, new MidiEventFactory(new Note(60), new Velocity(127), new Speed(127), new ResolutionSource(480), new Tick(0)).createmidievent_drum_4beat());

			MidiTrack midiTrack = new MidiTrack(midiEventCollection.toList());
			midiTrack.SetChannel(0);

			TrackCollection midiTracks = new TrackCollection();
			midiTracks.Add(new Bass(midiTracks).toMidiTrack());
			//midiTracks.Add(new Drum().toMidiTrack());

			//midiファイルの作成
			MidiWriter.WriteTo(DateTime.Now.ToString("yyyy-MM-dd-HH-MM-ss-ffffff") + ".mid", new MidiData(midiTracks, Constant.resolutionSource));
		}
	}
}
