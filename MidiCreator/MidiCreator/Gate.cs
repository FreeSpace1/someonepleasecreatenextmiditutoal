﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {
	class Gate {
		private int value;
		public Gate(int value) {
			this.value = value;
		}
		public int toInt() {
			return this.value;
		}

	}
}
