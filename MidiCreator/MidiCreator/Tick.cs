﻿using System;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.DataElement;
using NextMidi.DataElement.MetaData;

namespace MidiCreator {
	class Tick {
		private int value;
		public Tick(int value) {
			this.value = value;
		}
		public Tick(Tick tick, ResolutionSource resolutionSource) : this(tick, resolutionSource.Resolution) { }
		public Tick(Tick tick, int value) : this(tick) {
			this.value += value;
		}
		public Tick(Tick tick) : this(tick.value) { }
		public Tick(MidiTrack midiTrack) : this(midiTrack.TickLength) { }
		public Tick(MidiEvent midiEvent, Tick tick) : this(tick) {
			this.value += midiEvent.Tick;
		}
		public int toInt() {
			return this.value;
		}
		public bool isOver(int value) {
			return this.value > value;
		}
		public bool isOver(Tick tick) {
			return this.isOver(tick.value);
		}
		public bool isOver(MidiEndOfTrack midiEndOfTrack) {
			return this.isOver(midiEndOfTrack.Tick);
		}
	}
}