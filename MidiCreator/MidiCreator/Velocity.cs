﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {
	class Velocity {
		private byte value;
		public Velocity(byte value) {
			this.value = value;
		}
		public byte toByte() {
			return this.value;
		}

	}
}
