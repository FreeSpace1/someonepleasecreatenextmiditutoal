﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.DataElement.MetaData;

namespace MidiCreator {
	class Bass : Instrument {

		public Bass(TrackCollection trackCollection, ResolutionSource resolutionSource, MidiEndOfTrack midiEndOfTrack) : base(trackCollection, resolutionSource, midiEndOfTrack) {
			this.patternCollection.createBass();
		}

		public Bass(TrackCollection trackCollection) : base(trackCollection) {
			this.patternCollection = new PatternCollection();
			this.patternCollection.createBass();
		}

		public Bass(Channel channel) : base(channel){
			this.patternCollection.createBass();
		}
	}
}
																		   