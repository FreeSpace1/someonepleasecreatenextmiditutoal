﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {
	class Note {
		private byte value;
		public Note(byte value) {
			this.value = value;
		}
		public Note(int value) {
			Note note = new Note((byte)this.value);
			this.value = note.value;
		}
		public byte toByte() {
			return this.value;
		}
	}
}
