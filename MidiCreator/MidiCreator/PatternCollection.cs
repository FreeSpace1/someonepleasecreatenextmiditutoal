﻿using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.DataElement;
using NextMidi.DataElement.MetaData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {
	class PatternCollection {
		List<Pattern> value;

		public PatternCollection() {
			this.value = new List<Pattern>();
		}

		public PatternCollection createBass() {
			Random random = new Random();
			while (this.toMeasureCollection().Count < Constant.MeasureQuantity) {
				var patternQuantity = random.Next(Constant.MeasureQuantity);
				for (var index = 0; index < patternQuantity; index++) {
					this.value.Add(new Pattern(new Tick(this.toMidiTrack().TickLength)).createBass());
				}
			}
			return this;
		}

		private bool isOverEnd() {
			return this.getEndTick().isOver(Constant.tickEnd);
		}


		private Tick getEndTick() {
			return new Tick(this.toMidiTrack());
		}

		public MidiTrack toMidiTrack() {
			return new MidiTrack(this.toMidiEventList());
		}

		public List<MidiEvent> toMidiEventList() {
			List<MidiEvent> midiEventList = new List<MidiEvent>();
			foreach (var pattern in this.value) {
				midiEventList.AddRange(pattern.toMidiEventList());
			}
			return midiEventList;
		}

		//private List<MidiEvent> removeDuplicate() {
				
		//}


		private MeasureCollection toMeasureCollection() {
			MeasureCollection measureCollection = new MeasureCollection();
			foreach (var pattern in this.value) {
				measureCollection = new MeasureCollection(measureCollection, pattern);
			}
			return measureCollection;
		}

	}
}
