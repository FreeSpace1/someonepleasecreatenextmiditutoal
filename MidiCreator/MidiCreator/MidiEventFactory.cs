﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi;
using NextMidi.DataElement.MetaData;
using NextMidi.DataElement;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.Filing.Midi;
using NextMidi.Filing.Midi.MidiFile;

namespace MidiCreator {
	class MidiEventFactory {
		protected Note note;
		protected Velocity velocity;
		protected Speed speed;
		protected Tick tick;
		protected ResolutionSource resolutionSource;

		public MidiEventFactory(Note note, Velocity velocity, Speed speed, ResolutionSource resolutionSource, Tick tick) {
			this.note = note;
			this.velocity = velocity;
			this.speed = speed;
			this.resolutionSource = resolutionSource;
			this.tick = tick;
		}

		public MidiEventFactory(Note note, Velocity velocity, Speed speed, Tick tick) {
			MidiEventFactory midiEventFactory = new MidiEventFactory(note, velocity, speed, Constant.resolutionSource, tick);
			this.note = midiEventFactory.note;
			this.velocity = midiEventFactory.velocity;
			this.speed = midiEventFactory.speed;
			this.tick = midiEventFactory.tick;
			this.resolutionSource = midiEventFactory.resolutionSource;
		}
		private MidiEvent createNoteEvent( Gate gate ) {
			NoteEvent noteEvent = new NoteEvent(this.note.toByte(), this.velocity.toByte(), gate.toInt() , this.speed.toByte());
			noteEvent.Tick = this.tick.toInt();
			return noteEvent;
		}

		public MidiEvent createNoteEvent_MeasurePer1() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution * 4));
		}
		public MidiEvent createNoteEvent_MeasurePer2() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution * 2));
		}
		public MidiEvent createNoteEvent_MeasurePer4() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution * 1));
		}
		public MidiEvent createNoteEvent_MeasurePer8() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution / 2));
		}
		public MidiEvent createNoteEvent_MeasurePer16() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution / 4));
		}
		public MidiEvent createNoteEvent_MeasurePer32() {
			return this.createNoteEvent(new Gate(this.resolutionSource.Resolution / 8));
		}

		public MidiEvent NoteEvent_Random {
			get {
				var random = new Random().Next(6);
				if (random.Equals(0)) {
					return this.createNoteEvent_MeasurePer1();
				}
				if (random.Equals(1)) {
					return this.createNoteEvent_MeasurePer2();
				}
				if (random.Equals(2)) {
					return this.createNoteEvent_MeasurePer4();
				}
				if (random.Equals(3)) {
					return this.createNoteEvent_MeasurePer8();
				}
				if (random.Equals(4)) {
					return this.createNoteEvent_MeasurePer16();
				}
				return this.createNoteEvent_MeasurePer32();
			}
		}

		public MidiEventCollection createmidievent_drum_4beat() {
			MidiEventCollection midiEventCollection = new MidiEventCollection(new MidiEventFactory(new Note(38), this.velocity, this.speed, this.resolutionSource, this.tick).createNoteEvent_MeasurePer2());
			midiEventCollection = new MidiEventCollection(midiEventCollection, new MidiEventFactory(new Note(40), this.velocity,  this.speed, this.resolutionSource, new Tick(this.tick, this.resolutionSource)).createNoteEvent_MeasurePer4());
			midiEventCollection = new MidiEventCollection(midiEventCollection, new MidiEventFactory(new Note(38), this.velocity,  this.speed, this.resolutionSource, new Tick(this.tick, this.resolutionSource.Resolution * 2)).createNoteEvent_MeasurePer8());
			midiEventCollection = new MidiEventCollection(midiEventCollection, new MidiEventFactory(new Note(40), this.velocity,  this.speed, this.resolutionSource, new Tick(this.tick, this.resolutionSource.Resolution * 3)).createNoteEvent_MeasurePer1());
			return midiEventCollection;
		}


	}
}
