﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {
	class Speed {
		private byte value;
		public Speed(byte value) {
			this.value = value;
		}
		public byte toByte() {
			return this.value;
		}
	}
}
