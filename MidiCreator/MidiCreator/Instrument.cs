﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.DataElement.MetaData;

namespace MidiCreator {
	abstract class Instrument {
		private Channel channel;
		protected PatternCollection patternCollection;

		public Instrument(TrackCollection trackCollection, ResolutionSource resolutionSource, MidiEndOfTrack midiEndOfTrack) : this(new Channel((byte)trackCollection.Count), resolutionSource, midiEndOfTrack) { }
		public Instrument(TrackCollection trackCollection) : this(new Channel((byte)trackCollection.Count)) { }

		public Instrument(Channel channel, ResolutionSource resolutionSource, MidiEndOfTrack midiEndOfTrack) : this(channel) {	}

		public Instrument(Channel channel) {
			this.channel = channel;
		}


		public MidiTrack toMidiTrack() {
			var midiTrack = this.patternCollection.toMidiTrack();
			midiTrack.SetChannel(this.channel.toByte());
			return midiTrack;
		}

	}
}
