﻿using System;
using System.Collections.Generic;
using NextMidi.Data;
using NextMidi.DataElement;

namespace MidiCreator {
	abstract class MeasureBase {
		protected MidiEventCollection midiEventCollection;
		private ResolutionSource resolutionSource;
		public MeasureBase(MidiEventCollection midiEventCollection, ResolutionSource resolutionSource) : this(midiEventCollection) {
			this.resolutionSource = resolutionSource;
		}
		public MeasureBase(MidiEventCollection midiEventCollection) {
			this.midiEventCollection = midiEventCollection;
			this.resolutionSource = Constant.resolutionSource;
		}
		public MeasureBase() {
			this.midiEventCollection = new MidiEventCollection();
		}
		public MeasureBase(MidiEventCollection midiEventCollection, Tick tick) : this(midiEventCollection) {
			this.midiEventCollection = new MidiEventCollection(midiEventCollection, tick);
		}
		public MeasureBase(MeasureBase measureBass, Tick tick) : this(measureBass.midiEventCollection, tick) { }

		protected bool isOver1Measure() {
			if (this.midiEventCollection.toList().Count.Equals(0)) {
				return false;
			}
			return (Constant.resolutionSource.Resolution * 4) > this.midiEventCollection.differenceOfLastAndFirst();
		}

		internal IEnumerable<MidiEvent> toMidiEventList() {
			return this.midiEventCollection.toMidiEventList();
		}

		protected bool is1Measure() {
			if (this.midiEventCollection.toList().Count.Equals(0)) {
				return true;
			}
			return (Constant.resolutionSource.Resolution * 4) > this.midiEventCollection.differenceOfEndAndFirst();
		}

		public abstract MeasureBase createBass();

	}
}
