﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi;
using NextMidi.DataElement.MetaData;
using NextMidi.DataElement;
using NextMidi.Data;
using NextMidi.Data.Track;
using NextMidi.Filing.Midi;
using NextMidi.Filing.Midi.MidiFile;

namespace MidiCreator {
	class Drum : Instrument {
		public Drum(ResolutionSource resolutionSource, MidiEndOfTrack midiEndOfTrack) : base(new Channel(9), resolutionSource, midiEndOfTrack) { }
		public Drum() : base(new Channel(9)) { }

		//public MidiTrack createMidiTrack() {
		//	return this.patternCollection.createMidiTrack_Drum();
		//}


	}
}
																																	  