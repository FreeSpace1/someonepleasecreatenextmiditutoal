﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NextMidi.Data.Track;
using NextMidi.DataElement;

namespace MidiCreator {
	class MeasureCollection {
		List<MeasureBase> value;

		public MeasureCollection() {
			this.value = new List<MeasureBase>();
		}

		public MeasureCollection(MeasureCollection measureCollection, Pattern pattern) : this(measureCollection) {
			this.value.AddRange(pattern.toMeasureList());
		}
		public MeasureCollection(MeasureCollection measureCollection) {
			this.value = measureCollection.value;
		}
		public MeasureCollection(MeasureCollection measureCollection, Tick tick) : this(measureCollection) {
			var measureList = new List<MeasureBase>();
			foreach (var measure in this.value) {
				measureList.Add(this.toConcrete(measure, tick));
			}
			this.value = measureList;
		}

		public MeasureCollection(Tick tick) : this() {
			var measureList = new List<MeasureBase>();
			foreach (var measure in this.value) {
				measureList.Add(this.toConcrete(measure, tick));
			}
			this.value = measureList;

		}

		internal MeasureCollection createBass() {
			Random random = new Random();
			while (this.value.Count < Constant.MeasureQuantity) {
				var patternQuantity = random.Next(Constant.MeasureQuantity);
				for (var index = 0; index < patternQuantity; index++) {
					if (index.Equals(patternQuantity - 1)) {
						this.value.Add(new PeriodMeasure().createBass());
					}
					this.value.Add(new Measure().createBass());
				}
			}
			return this;
		}

		internal List<MidiEvent> toMidiEventList() {
			var midiEventList = new List<MidiEvent>();
			foreach (var measureBass in this.value) {
				midiEventList.AddRange(measureBass.toMidiEventList());
			}
			return midiEventList;
		}

		internal int Count {
			get {
				return this.value.Count;
			}
		}

		public List<MeasureBase> toMeasureList() {
			return this.value;
		}

		private MeasureBase toConcrete(MeasureBase measureBass, Tick tick) {
			if (measureBass.GetType() == new Measure().GetType())
				return new Measure(measureBass, tick);
			return new PeriodMeasure(measureBass, tick);
		}
	}


}
