﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidiCreator {

	class Channel {
		private byte value;
		public Channel(byte value) {
			this.value = value;
		}
		public byte toByte() {
			return this.value;
		}

	}
}
